package collections

/**
  * Some data structure to compute a approximate message density
  */

object DensityParameters {
  val THRESHOLD = 1
  val SIZE = 7
}

class DensityQueue(size: Int) {
  val content: Array[Option[Long]] = Array.fill[Option[Long]](size)(None)
  var mostRecent = -1

  def enqueue(element: Long) = {
    content((mostRecent + 1) % size) = Some(element)
    mostRecent = (mostRecent + 1) % size
  }

  def density: Option[Double] = content((mostRecent + 1) % size) match {
    case Some(value) =>
      if (content.forall(_.isDefined))
        Some(size.toDouble / (content(mostRecent).get - value))
      else None
    case None => None
  }
}
