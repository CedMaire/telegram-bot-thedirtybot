package main

/////////////////////////////////////
// Telegrambot API & Scala imports //
/////////////////////////////////////

import java.nio.file.FileSystems

import core.Responses.{DES_BARRES, _}
import info.mukel.telegrambot4s.Implicits._
import info.mukel.telegrambot4s.api.{Commands, Polling, TelegramBot}
import info.mukel.telegrambot4s.methods.SendMessage
import info.mukel.telegrambot4s.models._
import main.BotSmallTalk._
import main.OwnCommands._

import scala.io.{Codec, Source}
import scala.util.Random

/////////////////////
// Project imports //
/////////////////////

import core.{Media, TransformationsManager}

/**
  * Created with love by Cedric Maire, Antonio Morais & Xavier Pantet.
  * Birthdate: 9th of April 2017
  */

object TheDirtyBot extends App with TelegramBot with Polling with Commands {
  ////////////////////////////////
  // Some easy shit definitions //
  ////////////////////////////////

  lazy val token: String = Source.fromFile(TOKEN_PATH)(Codec.UTF8).getLines().mkString
  val TIME_DIV: Long = 1000L
  val START_UP_DATE: Long = System.currentTimeMillis() / TIME_DIV

  /////////////////////////////
  // Write the commands here //
  /////////////////////////////

  on(CMD_HI, CMD_HI_DESC) {
    implicit msg => _ => {
      val rng = new Random(System.currentTimeMillis())

      reply(HIs(rng.nextInt(HIs.size)), replyToMessageId = msg.messageId)
    }
  }
  on(CMD_BIRTH, CMD_BIRTH_DESC) { implicit msg => _ => reply(BIRTH) }
  on(CMD_REVERSE, CMD_REVERSE_DESC) { implicit msg => args => reply(args.mkString(" ").reverse) }
  on(CMD_SECRET, CMD_SECRET_DESC) { implicit msg => _ => reply(DES_BARRES_HORI, replyMarkup = Some(COSTUM_KEYBOARD)) }
  on(CMD_DES_BARRES, CMD_DES_BARRES_DESC) { implicit msg => _ => reply(DES_BARRES) }

  /**
    * onMessage handles new incoming messages and decides whether
    * it should let the API treat it as a command or delegate it
    * to the transformation manager.
    *
    * @param message the message we have to process
    */
  override def onMessage(message: Message): Unit = {
    if (isRecent(message)) {
      message.text match {
        case Some(text) if text.charAt(0) == CMD_CHAR => super.onMessage(message)
        case Some(text) => send(message, TransformationsManager.handle(message))
        case _ => println(NOTHING_TO_DO)
      }
    }
  }

  /**
    * Send the data to the client
    */
  def send(message: Message, data: (Option[String], Option[Media])): Unit = {
    data match {
      case (None, None) => println(NOTHING_TO_DO)
      case (Some(text: String), None) => replyTo(message, text)
      case (None, Some(media: Media)) => sendMedia(message.source, media)
      case (Some(text: String), Some(media: Media)) =>
        replyTo(message, text)
        sendMedia(message.source, media)
    }
  }

  /**
    * Allows to reply to a certain message with a given response.
    */
  def replyTo(message: Message, text: String): Unit = request(SendMessage(message.source, text))

  /**
    * Allows to send a certain media with a given id or path.
    */
  def sendMedia(source: Long, media: Media) = (media.id, media.path) match {
    case (Some(id), _) => request(media.sendMedia(source, id))
    case (_, Some(path)) => request(media.sendMedia(source, InputFile(FileSystems.getDefault.getPath(path))))
    case _ => println(NO_CONTENT)
  }

  /**
    * Allows us to check whether the message is recent enough to be processed.
    */
  def isRecent(message: Message): Boolean = message.date > START_UP_DATE

  ///////////////////
  // Run that bot! //
  ///////////////////

  super.run()
  println(BOT_GREETINGS)
}
