package main

import info.mukel.telegrambot4s.models.{KeyboardButton, ReplyKeyboardMarkup}

/**
  * Object to define some commands and their corresponding descriptions.
  */

object OwnCommands {
  //////////////
  // Commands //
  //////////////

  val CMD_CHAR = '/'

  val CMD_HI = "/hi"
  val CMD_HI_DESC = "Say hello!"

  val CMD_BIRTH = "/birth"
  val CMD_BIRTH_DESC = "BotFather didn't pull out in time..."

  val CMD_REVERSE = "/reverse"
  val CMD_REVERSE_DESC = "Reverse that shit!"

  val CMD_SECRET = "/secret"
  val CMD_SECRET_DESC = "The most secret, but also the most useful command."

  val CMD_DES_BARRES = "///"
  val CMD_DES_BARRES_DESC = "Send some \"barres\"!"

  ///////////////////////////
  // Buttons, Keyboards... //
  ///////////////////////////

  val DES_BARRES = CMD_DES_BARRES
  val DES_BARRES_BUTTON = KeyboardButton(DES_BARRES)

  val THROW_UP = "\uD83D\uDE37"
  val THROW_UP_BUTTON = KeyboardButton(THROW_UP)

  val THUMBS_UP = "\uD83D\uDC4D\uD83C\uDFFC"
  val THUMBS_UP_BUTTON = KeyboardButton(THUMBS_UP)

  val COFFEE = "☕️"
  val COFFEE_BUTTON = KeyboardButton(COFFEE)

  val HANDS_UP = "\uD83D\uDE4B\uD83C\uDFFB"
  val HANDS_UP_BUTTON = KeyboardButton(HANDS_UP)

  val FUCK = "\uD83D\uDD95\uD83C\uDFFC"
  val FUCK_BUTTON = KeyboardButton(FUCK)

  val ROW_01 = Seq(COFFEE_BUTTON, HANDS_UP_BUTTON)
  val ROW_02 = Seq(THROW_UP_BUTTON, DES_BARRES_BUTTON)
  val ROW_03 = Seq(FUCK_BUTTON, THUMBS_UP_BUTTON)

  val BUTTONS = Seq(ROW_01, ROW_02, ROW_03)
  val COSTUM_KEYBOARD = ReplyKeyboardMarkup(BUTTONS, resizeKeyboard = Some(false))
}
