package main

/////////////////////////////////////
// Telegrambot API & Scala imports //
/////////////////////////////////////

import java.util.Calendar

import info.mukel.telegrambot4s.models.Message

/**
  * Some set of configuration settings
  */

object BotSmallTalk {
  val TOKEN_PATH = "src/main/resources/infos/bot.token"

  def time = "[" + Calendar.getInstance().getTime.toString + "] - "

  def BOT_GREETINGS = time + "TheDirtyBot here, I was launched and they see me rollin'... they hatin'..."

  def NOTHING_TO_DO = time + "Nothing to do with this one..."

  def NO_CONTENT = time + "No content was received..."

  def printMediaID(message: Message) = {
    if (message.sticker.isDefined) println("Sticker ID: " + message.sticker.get.fileId)
    if (message.photo.isDefined) println("Photo ID: " + message.photo.get.head.fileId)
    if (message.audio.isDefined) println("Audio ID: " + message.audio.get.fileId)
    if (message.video.isDefined) println("Video ID: " + message.video.get.fileId)
    if (message.voice.isDefined) println("Voice ID: " + message.voice.get.fileId)
    if (message.document.isDefined) println("Document ID: " + message.document.get.fileId)
    if (message.videoNote.isDefined) println("VideoNote ID: " + message.videoNote.get.fileId)
  }
}
