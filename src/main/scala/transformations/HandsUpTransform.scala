package transformations

/////////////////////
// Project imports //
/////////////////////

import core.Responses._
import core._
import main.OwnCommands._

object HandsUpTransform extends Transformation {
  /////////////////////
  // Some properties //
  /////////////////////

  val HANDS_UP_TRIGGER = HANDS_UP

  /////////////////
  // Main method //
  ////////////////

  override def response(message: BotMessages): (Option[String], Option[Media]) = {
    if (message.getText.equals(HANDS_UP_TRIGGER)) (Some(PUT_HANDS_UP), None)
    else (None, None)
  }
}
