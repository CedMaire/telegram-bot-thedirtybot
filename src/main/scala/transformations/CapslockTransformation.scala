package transformations

//////////////////////////////////////
// Telegram bot API & Scala imports //
//////////////////////////////////////

/////////////////////
// Project imports //
/////////////////////

import core.Responses._
import core._

import scala.util.Random

object CapslockTransformation extends Transformation {
  /////////////////////
  // Some properties //
  /////////////////////

  val HOLY_PUSSY_FART_PATH = "src/main/resources/audio/holypussyfart.ogg"
  val I_AM_THE_ONE_PATH = "src/main/resources/audio/iamtheonetheone.ogg"
  val MIN_LENGTH = 4

  override def response(message: BotMessages): (Option[String], Option[Media]) = {
    /////////////////////
    // Some properties //
    /////////////////////

    val TEXT = message.getText
    val TEXT_UPPER_CASED = TEXT.toUpperCase
    val random = new Random(System.currentTimeMillis())

    /////////////////
    // Main method //
    /////////////////
    if (TEXT.length >= MIN_LENGTH && TEXT.equals(TEXT_UPPER_CASED))
      if (random.nextInt() % 2 == 0) (Some(HOLY_PUSSY_FART), Some(Voice(None, Some(HOLY_PUSSY_FART_PATH))))
      else (Some(I_AM_THE_ONE), Some(Voice(None, Some(I_AM_THE_ONE_PATH))))
    else (None, None)
  }
}
