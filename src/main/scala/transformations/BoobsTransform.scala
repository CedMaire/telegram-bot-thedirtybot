package transformations

//////////////////////////////////////
// Telegram bot API & Scala imports //
//////////////////////////////////////

import collections.DensityParameters._

import scala.collection.mutable

/////////////////////
// Project imports //
/////////////////////

import collections.DensityQueue
import core._

object BoobsTransform extends Transformation {
  /////////////////////
  // Some properties //
  /////////////////////

  val conv: mutable.Map[Long, DensityQueue] = mutable.Map()
  val RANDOM_BOOBS = "\uD83D\uDCF7 Random Boobs"
  val RANDOM_BOOTY = "\uD83C\uDF51 Random Booty"

  /////////////////
  // Main method //
  /////////////////
  override def response(message: BotMessages): (Option[String], Option[Media]) = {
    val idConv: Long = message.get.source

    conv.get(idConv) match {
      case Some(q) =>
        val text = message.getText
        if (text.equals(RANDOM_BOOBS) || text.equals(RANDOM_BOOTY)) {
          q.enqueue(message.get.date.toLong)
        }

        q.density match {
          case Some(d) => if (d > THRESHOLD) {
            conv.remove(idConv)
            (None, Some(Sticker(Some(MediaID.FAP_GUY), None)))
          }
          else {
            (None, None)
          }
          case _ => (None, None)
        }
      case None =>
        conv(idConv) = new DensityQueue(SIZE)
        (None, None)
    }
  }
}
