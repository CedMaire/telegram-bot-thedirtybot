package transformations

/////////////////////
// Project imports //
/////////////////////

import core.{BotMessages, Media, Responses, Transformation}

object BieneTransform extends Transformation {
  /////////////////////
  // Some properties //
  /////////////////////

  val BIENE = Responses.BIENE.toLowerCase

  /////////////////
  // Main method //
  /////////////////

  override def response(message: BotMessages): (Option[String], Option[Media]) = {
    if (message.getTLCSplittedNoPunct.contains(BIENE)) (Some(Responses.BIENE), None)
    else (None, None)
  }
}
