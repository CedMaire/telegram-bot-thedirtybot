package transformations

//////////////////////////////////////
// Telegram bot API & Scala imports //
//////////////////////////////////////

import scala.util.Random

/////////////////////
// Project imports //
/////////////////////

import core._


object SpongeBobTransform extends Transformation {
  /////////////////////
  // Some properties //
  /////////////////////

  val QUOTE = "\""

  /////////////////
  // Main method //
  /////////////////
  override def response(message: BotMessages): (Option[String], Option[Media]) = {
    val text = message.getTLC

    if (accept(message)) {
      val rng = new Random(System.currentTimeMillis())

      (Some(QUOTE ++ text.map((c: Char) => if (rng.nextBoolean()) c.toUpper else c.toLower) + QUOTE),
        Some(Sticker(Some(MediaID.SPONGE_MOCKING), None)))

    } else {
      (None, None)
    }
  }

  /////////////
  // HELPERS //
  /////////////

  def accept(message: BotMessages): Boolean = {
    val messageSplitted = message.getTLCSplittedNoPunct

    frenchEqual(messageSplitted) || englishEqual(messageSplitted)
  }

  def frenchEqual(messageSplitted: Seq[String]): Boolean = {
    messageSplitted.length > 1 && messageSplitted.head.equals("jaime") && messageSplitted(1).equals("pas")
  }

  def englishEqual(messageSplitted: Seq[String]): Boolean = {
    messageSplitted.length > 2 && messageSplitted.head.equals("i") &&
      messageSplitted(1).equals("dont") && messageSplitted(2).equals("like")
  }
}
