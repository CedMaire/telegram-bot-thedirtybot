package transformations

/////////////////////
// Project imports //
/////////////////////

import core.{BotMessages, Media, Transformation}

object AlsoKnownAsTransform extends Transformation {
  /////////////////////
  // Some properties //
  /////////////////////

  val MAX_FACT = 21
  val FACT_OP = "!"
  val STRING_START = "-> "
  val ALSO_KNOWN_AS = " which is also known as "
  val NEW_LINE = "\n"

  val locale = new java.util.Locale("de", "DE")
  val formatter = java.text.NumberFormat.getIntegerInstance(locale)

  def subFact(string: String): String = string.substring(0, string.length - 1)

  def isFact(string: String): Boolean = string.length > 1 &&
    string.endsWith(FACT_OP) &&
    subFact(string).forall((c: Char) => c.isDigit)

  def fact(n: Int): Double = if (n > 1) (1 to n).map((i: Int) => i.toDouble).product else 1

  /////////////////
  // Main method //
  /////////////////

  override def response(message: BotMessages): (Option[String], Option[Media]) = {
    val facts = message.getTextSplitted.toList.filter((s: String) => isFact(s))
      .map((s: String) => subFact(s).toInt)
      .filter((i: Int) => i <= MAX_FACT).sorted

    if (facts.nonEmpty) {
      val text = facts.map((i: Int) => STRING_START + i + FACT_OP + ALSO_KNOWN_AS + formatter.format(fact(i)) + NEW_LINE)
        .reduce(_ + _)

      (Some(text), None)
    } else (None, None)
  }
}
