package transformations

/////////////////////
// Project imports //
/////////////////////

import core.{BotMessages, Media, Responses, Transformation}

object PasFauxTransform extends Transformation {
  /////////////////////
  // Some properties //
  /////////////////////

  val VRAI = "vrai"

  /////////////////
  // Main method //
  /////////////////
  override def response(message: BotMessages): (Option[String], Option[Media]) = {
    if (message.getTLCSplittedNoPunct.contains(VRAI)) (Some(Responses.CEST_PAS_FAUX), None) else (None, None)
  }
}
