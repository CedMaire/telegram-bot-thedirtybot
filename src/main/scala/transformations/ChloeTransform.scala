package transformations

/////////////////////
// Project imports //
/////////////////////

import java.text.Normalizer

import core.BotMessages._
import core.{BotMessages, Media, Responses, Transformation}

object ChloeTransform extends Transformation {
  /////////////////////
  // Some properties //
  /////////////////////

  val CHLOE = "chloe"
  val ACCENT_REGEX = "\\p{M}"

  /////////////////
  // Main method //
  /////////////////
  override def response(message: BotMessages): (Option[String], Option[Media]) = {
    val normalizedWords = Normalizer.normalize(message.getTLC, Normalizer.Form.NFD)
      .replaceAll(ACCENT_REGEX, EMPTY_STRING).split(SPLIT_CHAR)

    if (normalizedWords.contains(CHLOE)) (Some(Responses.CHLOE), None)
    else (None, None)
  }
}
