package transformations

/////////////////////
// Project imports //
/////////////////////

import core._

object BoiTransform extends Transformation {
  /////////////////////
  // Some properties //
  /////////////////////

  val BOI_TRIGGER = "..."

  /////////////////
  // Main method //
  ////////////////

  override def response(message: BotMessages): (Option[String], Option[Media]) = {
    if (message.getTLC.equals(BOI_TRIGGER)) (None, Some(Sticker(Some(MediaID.SPONGE_BREATH_IN), None)))
    else (None, None)
  }
}
