package transformations

/////////////////////
// Project imports //
/////////////////////

import core._

object DamianTransform extends Transformation {
  /////////////////////
  // Some properties //
  /////////////////////

  val DAMIAN = "damian"

  /////////////////
  // Main method //
  /////////////////
  override def response(message: BotMessages): (Option[String], Option[Media]) = {
    if (message.getTLCSplittedNoPunct.contains(DAMIAN))
      (None, Some(Sticker(Some(MediaID.DAMIAN_CORRECTION), None)))
    else (None, None)
  }
}
