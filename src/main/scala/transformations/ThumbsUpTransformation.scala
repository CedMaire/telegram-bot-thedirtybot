package transformations

/////////////////////
// Project imports //
/////////////////////

import core.Responses._
import core._
import main.OwnCommands._

object ThumbsUpTransformation extends Transformation {
  /////////////////////
  // Some properties //
  /////////////////////

  val THUMBS_UP_TRIGGER = THUMBS_UP

  /////////////////
  // Main method //
  ////////////////

  override def response(message: BotMessages): (Option[String], Option[Media]) = {
    if (message.getText.equals(THUMBS_UP_TRIGGER)) (Some(ALRIGHT), None)
    else (None, None)
  }
}
