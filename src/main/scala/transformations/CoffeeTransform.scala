package transformations

/////////////////////
// Project imports //
/////////////////////

import core.Responses._
import core._
import main.OwnCommands._

import scala.util.Random

object CoffeeTransform extends Transformation {
  /////////////////////
  // Some properties //
  /////////////////////

  val COFFEE_TRIGGER = COFFEE

  /////////////////
  // Main method //
  ////////////////

  override def response(message: BotMessages): (Option[String], Option[Media]) = {
    if (message.getText.equals(COFFEE_TRIGGER)) {
      val rng = new Random(System.currentTimeMillis())

      (Some(COFFEES(rng.nextInt(COFFEES.size))), None)
    }
    else (None, None)
  }
}
