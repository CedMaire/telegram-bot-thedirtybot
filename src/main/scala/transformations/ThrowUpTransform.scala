package transformations

/////////////////////
// Project imports //
/////////////////////

import core._
import main.OwnCommands._

object ThrowUpTransform extends Transformation {
  /////////////////////
  // Some properties //
  /////////////////////

  val THROW_UP_TRIGGER = THROW_UP

  /////////////////
  // Main method //
  ////////////////

  override def response(message: BotMessages): (Option[String], Option[Media]) = {
    if (message.getText.equals(THROW_UP_TRIGGER)) (None, Some(Document(Some(MediaID.THROW_UP), None)))
    else (None, None)
  }
}
