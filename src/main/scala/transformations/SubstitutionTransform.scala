package transformations

/////////////////////
// Project imports //
/////////////////////

import core.BotMessages._
import core.{BotMessages, Transformation}

object SubstitutionTransform extends Transformation {
  /////////////////////
  // Some properties //
  /////////////////////

  val ID_MAPPER = 10
  val TWO_EMOJI = "✌\uD83C\uDFFC"
  val PENIS_BALLZ = "8="
  val PENIS_STICK = '='
  val PENIS_HEAD = "D"
  val deuxVariants: Array[String] = Array("de", "deux", "2")
  val biteVariants: Array[String] = Array("bite", "chibre", "zizi", "bites", "chibres", "zizis", "queue", "queues",
    "teube", "teubes", "que", "dick")

  /////////////////
  // Main method //
  /////////////////
  override def response(message: BotMessages) = {
    val messageText: Array[String] = message.getTextSplitted
    val text: String = messageText.map((word: String) => {
      val cleanWord = word.toLowerCase.replaceAll(toDeleteRegex, EMPTY_STRING)

      if (deuxVariants.contains(cleanWord)) TWO_EMOJI
      else if (biteVariants.contains(cleanWord)) length(message) match {
        case Some(str) => PENIS_BALLZ + str + PENIS_HEAD
        case None => None
      }
      else word
    }).mkString(" ")

    if (text != messageText.mkString(" ")) (Some(text), None) else (None, None)
  }

  /////////////
  // HELPERS //
  /////////////
  def length(message: BotMessages): Option[String] = message.get.from match {
    case Some(i) => Some(Array.fill[Char](i.id % ID_MAPPER)(PENIS_STICK).mkString)
    case None => None
  }
}
