package transformations

/////////////////////
// Project imports //
/////////////////////

import core._

object PierrePierreTransform extends Transformation {
  /////////////////////
  // Some properties //
  /////////////////////

  val NUM_PIERRES_WANTED = 2
  val PIERRE_PIERRES: Array[String] = Array("pierre")

  /////////////////
  // Main method //
  /////////////////
  override def response(message: BotMessages): (Option[String], Option[Media]) = {
    val numPierres = message.getTLCSplittedNoPunct.count((s: String) => PIERRE_PIERRES.contains(s))

    if (numPierres >= NUM_PIERRES_WANTED) (None, Some(Sticker(Some(MediaID.SPONGE_CAVE_MAN), None)))
    else (None, None)
  }
}
