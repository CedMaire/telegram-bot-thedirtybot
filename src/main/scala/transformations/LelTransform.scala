package transformations

/////////////////////
// Project imports //
/////////////////////

import core._

object LelTransform extends Transformation {
  /////////////////////
  // Some properties //
  /////////////////////

  val LEL_TRIGGER = "lel"

  /////////////////
  // Main method //
  ////////////////

  override def response(message: BotMessages): (Option[String], Option[Media]) = {
    if (message.getTLCNoPunct.equals(LEL_TRIGGER))
      (None, Some(Sticker(Some(MediaID.LOL), None)))
    else (None, None)
  }
}
