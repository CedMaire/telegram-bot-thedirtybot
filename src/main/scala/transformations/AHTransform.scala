package transformations

//////////////////////////////////////
// Telegram bot API & Scala imports //
//////////////////////////////////////

import collections.DensityParameters._

import scala.collection.mutable

/////////////////////
// Project imports //
/////////////////////

import collections.DensityQueue
import core.{BotMessages, Media, Responses, Transformation}

object AHTransform extends Transformation {
  /////////////////////
  // Some properties //
  /////////////////////

  val conv: mutable.Map[Long, DensityQueue] = mutable.Map()

  /////////////////
  // Main method //
  /////////////////
  override def response(message: BotMessages): (Option[String], Option[Media]) = {
    val idConv: Long = message.get.source

    conv.get(idConv) match {
      case Some(q) =>
        q.enqueue(message.get.date.toLong)

        q.density match {
          case Some(d) => if (d > THRESHOLD) {
            conv.remove(idConv)
            (Some(Responses.AH), None)
          }
          else {
            (None, None)
          }
          case _ => (None, None)
        }
      case None =>
        conv(idConv) = new DensityQueue(SIZE)
        (None, None)
    }
  }
}
