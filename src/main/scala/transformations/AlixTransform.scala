package transformations

/////////////////////
// Project imports //
/////////////////////

import core.{BotMessages, Media, Responses, Transformation}

object AlixTransform extends Transformation {
  /////////////////////
  // Some properties //
  /////////////////////

  val ALIX = "alix"

  /////////////////
  // Main method //
  /////////////////

  override def response(message: BotMessages): (Option[String], Option[Media]) = {
    if (message.getTLCSplittedNoPunct.contains(ALIX)) (Some(Responses.ALIX), None)
    else (None, None)
  }
}
