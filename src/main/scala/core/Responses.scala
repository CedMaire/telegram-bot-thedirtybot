package core

import main.OwnCommands._

/**
  * Some predefined responses
  */

object Responses {
  val HIs = List("Grüezi!", "Hi!", "Hello!", "Salut!", "Bonjour!", "Bom dia!", "Buenos dias!", "Hallo!", "Kon'nichiwa!",
    "Haai", "Goeie dag", "Kamusta", "Buschur", "Salam əleyküm", "Oi", "Guuten takh!", "Goededag", "Salama alaikum",
    "Moien", "Gwe’", "K'uxi", "Salawmaleýkim", "Demê şıma be xeyr")

  val ALIX = "Alix! Tricheur! Jamais je m'assoie à coté de toi en SHS!"

  val BIRTH =
    "Mes parents sont Cedric Maire, Antonio Morais & Xavier Pantet. Je suis né le 9 avril 2017 et de sexe féminin. Ouiiiiii!"

  val AH = "AH!"

  val CEST_PAS_FAUX = "Ouais, c'est pas faux..."

  val CHLOE = "I came here pleased to meet you, with some good meat to please you. - [Matthieu Buot de son Épine to Chloé]"

  val BIENE = "BIENE"

  val HOLY_PUSSY_FART = "HOLY PUSSY FART!..."

  val I_AM_THE_ONE = "I AM THE ONE, DON'T WEIGH A TON, DON'T NEED A GUN TO GET RESPECT UP ON THE STREET!..."

  val DES_BARRES = "///"
  val DES_BARRES_HORI = "≣"

  val ALRIGHT = "Alright n'gge! " + THUMBS_UP

  val COFFEES = List("Café? " + COFFEE, "K Fé? " + COFFEE, "Covfefe? " + COFFEE, "Ka Fé? " + COFFEE, "Coffee? " + COFFEE,
    "Quaffé? " + COFFEE, "Kaffé? " + COFFEE, "Quah Fé? " + COFFEE, "Kofi? " + COFFEE, "Koh Fi? " + COFFEE)

  val PUT_HANDS_UP = "Levez la main s'il vous plaît. " + HANDS_UP
}
