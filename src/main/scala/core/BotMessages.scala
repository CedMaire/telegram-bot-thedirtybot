package core

/////////////////////////////////////
// Telegrambot API & Scala imports //
/////////////////////////////////////

import core.BotMessages.{EMPTY_STRING, SPLIT_CHAR, toDeleteRegex}
import info.mukel.telegrambot4s.models.Message

/**
  * This class is an extension of the Message class
  * of the Telegram bot API. It provides few useful method to
  * parse message content.
  */

class BotMessages(private val message: Message) {
  def get = message

  def getText = get.text.get

  def getTLC = getText.toLowerCase()

  def getTextNoPunct = getText.replaceAll(toDeleteRegex, EMPTY_STRING)

  def getTLCNoPunct = getTLC.replaceAll(toDeleteRegex, EMPTY_STRING)

  def getTextSplitted = getText.split(SPLIT_CHAR)

  def getTLCSplitted = getTLC.split(SPLIT_CHAR)

  def getTextSplittedNoPunct = getTextNoPunct.split(SPLIT_CHAR)

  def getTLCSplittedNoPunct = getTLCNoPunct.split(SPLIT_CHAR)
}

object BotMessages {
  val SPLIT_CHAR = ' '
  val EMPTY_STRING = ""
  val toDeleteRegex = "[^\\w\\s]"

  def apply(message: Message): BotMessages = new BotMessages(message)
}
