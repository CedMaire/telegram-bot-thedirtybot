package core

/**
  * Simple trait that enforces transformations to have a response
  * method in common.
  */

trait Transformation {
  def response(message: BotMessages): (Option[String], Option[Media])
}
