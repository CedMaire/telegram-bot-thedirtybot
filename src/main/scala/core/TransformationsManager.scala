package core

//////////////////////////////////////
// Telegram bot API & Scala imports //
//////////////////////////////////////

import info.mukel.telegrambot4s.models.Message

/////////////////////
// Project imports //
/////////////////////

import transformations._

object TransformationsManager {
  /////////////////////
  // Transformations //
  /////////////////////

  lazy val transformations: List[Transformation] = List(
    BoobsTransform,
    AHTransform,
    BoiTransform,
    LelTransform,
    ThrowUpTransform,
    ThumbsUpTransformation,
    CoffeeTransform,
    HandsUpTransform,
    FuckTransform,
    BieneTransform,
    AlsoKnownAsTransform,
    PasFauxTransform,
    SpongeBobTransform,
    DamianTransform,
    PierrePierreTransform,
    AlixTransform,
    SubstitutionTransform,
    ChloeTransform)

  /////////////////
  // Main method //
  /////////////////    
  def handle(message: Message): (Option[String], Option[Media]) = {
    val text: Option[String] = message.text

    val t: (Boolean, (Option[String], Option[Media])) = transformations.foldLeft((false, (text, Option.empty[Media])))(
      (acc: (Boolean, (Option[String], Option[Media])), transform: Transformation) => {
        if (!acc._1) {
          val r = transform.response(BotMessages(message))

          if (r._1.isDefined || r._2.isDefined) (true, (r._1, r._2)) else (false, acc._2)
        }
        else (true, acc._2)
      }

    )
    if (t._1) (t._2._1, t._2._2) else (None, None)
  }
}
