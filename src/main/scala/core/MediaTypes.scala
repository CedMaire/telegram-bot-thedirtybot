package core

/////////////////////////////////////
// Telegrambot API & Scala imports //
/////////////////////////////////////

import core.MediaTypes.{MediaID, MediaPath}
import info.mukel.telegrambot4s.methods._
import info.mukel.telegrambot4s.models.{InputFile, Message}

/**
  * Some types definitions
  */

object MediaTypes {
  type MediaID = String
  type MediaPath = String
}

/**
  * This class stands on top of any media that can be sent to the clients
  */
abstract class Media(val id: Option[MediaID], val path: Option[MediaPath]) {
  def sendMedia(source: Either[Long, String], media: Either[InputFile, String]): ApiRequestMultipart[Message]
}

/**
  * Photos that can be sent
  */
case class Photo(override val id: Option[MediaID], override val path: Option[MediaPath]) extends Media(id, path) {
  def sendMedia(source: Either[Long, String], photo: Either[InputFile, String]) = SendPhoto(source, photo)
}

/**
  * Videos that can be sent
  */
case class Video(override val id: Option[MediaID], override val path: Option[MediaPath]) extends Media(id, path) {
  def sendMedia(source: Either[Long, String], video: Either[InputFile, String]) = SendVideo(source, video)
}

/**
  * Audios that can be sent
  */
case class Audio(override val id: Option[MediaID], override val path: Option[MediaPath]) extends Media(id, path) {
  def sendMedia(source: Either[Long, String], audio: Either[InputFile, String]) = SendAudio(source, audio)
}

/**
  * Voices that can be sent
  */
case class Voice(override val id: Option[MediaID], override val path: Option[MediaPath]) extends Media(id, path) {
  def sendMedia(source: Either[Long, String], voice: Either[InputFile, String]) = SendVoice(source, voice)
}

/**
  * Stickers that can be sent
  */
case class Sticker(override val id: Option[MediaID], override val path: Option[MediaPath]) extends Media(id, path) {
  def sendMedia(source: Either[Long, String], sticker: Either[InputFile, String]) = SendSticker(source, sticker)
}

/**
  * Documents that can be sent
  */
case class Document(override val id: Option[MediaID], override val path: Option[MediaPath]) extends Media(id, path) {
  def sendMedia(source: Either[Long, String], document: Either[InputFile, String]) = SendDocument(source, document)
}
