package core

/**
  * Some predefined stickers IDs
  */

object MediaID {
  //////////////
  // Stickers //
  //////////////

  val LOL = "CAADBAADAQADaWAFFWJqQHTp8GDoAg"

  val FEEL_LIKE_SIR = "CAADBAADAgADaWAFFe35RTYpd7NDAg"

  val FAP_GUY = "CAADBAADAwADaWAFFfG_BcB1f-oqAg"

  val DAMIAN_CORRECTION = "CAADBAADBQADaWAFFWse5LpxfuHmAg"

  val SPONGE_BREATH_IN = "CAADBAADBgADaWAFFQHdcBSoSQ5lAg"

  val SPONGE_CAVE_MAN = "CAADBAADBwADaWAFFYpvOnHIZ1vQAg"

  val SPONGE_MOCKING = "CAADBAADCAADaWAFFXZ5QGcBFunFAg"

  val PROFILE_PIC = "CAADBAADCQADaWAFFSKGY1IT-Gn4Ag"

  val FUCK = "CAADBAADCgADaWAFFbzyIbLC1EKuAg"

  //////////
  // GIFs //
  //////////

  val THROW_UP = "CgADBAADFgMAAj4YZAd_G_aY0Uu97gI"
}
