BotFather was really a good father. Now that I'm standalone, try to find all my secrets...

The dirtiest of all the bots out there!

Tags: release-X.X
Current: 1.1

---

Launch Bot:
`setsid nohup sbt run &`

Kill process:
```bash
$ top command
$ locate USER xavier, name java
$ kill corresponding process
```

---

Stickers IDs:
```
correction:CAADBAADBQADaWAFFWse5LpxfuHmAg
fap:CAADBAADAwADaWAFFfG_BcB1f-oqAg
feels-like-a-sir:CAADBAADAgADaWAFFe35RTYpd7NDAg
fuck:CAADBAADCgADaWAFFbzyIbLC1EKuAg
lol:CAADBAADAQADaWAFFWJqQHTp8GDoAg
profile:CAADBAADCQADaWAFFSKGY1IT-Gn4Ag
spongebob-breath-in:CAADBAADBgADaWAFFQHdcBSoSQ5lAg
spongebob-caveman:CAADBAADBwADaWAFFYpvOnHIZ1vQAg
spongebob-mocking:CAADBAADCAADaWAFFXZ5QGcBFunFAg
```

GIF IDs:

`throw-up:CgADBAADFgMAAj4YZAd_G_aY0Uu97gI`

---

Libraries:

* `https://core.telegram.org/bots/api`
* `https://github.com/mukel/telegrambot4s`
* `https://github.com/mukel/StarterBot`
